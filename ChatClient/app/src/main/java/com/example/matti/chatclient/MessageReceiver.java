package com.example.matti.chatclient;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created by Matti on 17.9.2015.
 */
public class MessageReceiver implements Runnable {
    private Socket socket;
    private Handler handler;
    private Message msg;
    private String receivedInput = "";
    private BufferedReader serverInput;
    private static final String TAG = "ChatClient";

    public MessageReceiver(Handler handler, Socket socket) {
        //receiving the socket and handler objects
        this.socket = socket;
        this.handler = handler;
        Log.e(TAG, "New MessageReceiver object was created.");
        Log.e(TAG, "Socket info in MessageReceiver " + this.socket.getInetAddress());
        Log.e(TAG, "Socket port in MessageReceiver " + this.socket.getLocalPort());


    }

    @Override
    public void run() {
        Log.e(TAG, "Run method started.");
        //creating a new BufferedReader
        try {
            this.serverInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // loop for receiving input from the chat server
        while (true) {
            //setting receivedInput to null
            this.receivedInput = null;
            try {
                Log.e(TAG, "Socket info in MessageReceiver loop " + this.socket.getInetAddress());
                Log.e(TAG, "Socket port in MessageReceiver loop " + this.socket.getLocalPort());
                Log.e(TAG, "Receiving message loop, reading input");
                // storing the input into receivedInput var
                this.receivedInput = this.serverInput.readLine();
                Log.e(TAG, "Input received is" + this.receivedInput);
                // checking if the received input was null
                // if null, the app will close
                if (this.receivedInput.length() <= 0 || this.receivedInput == null) {
                    Log.e(TAG, "The input was null.");
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (this.receivedInput != null) {
                Log.e(TAG, "Putting received message " + this.receivedInput + " into Message obj");
                //getting message from handler
                this.msg = handler.obtainMessage();
                this.msg.what = 0;
                this.msg.obj = receivedInput;
                //sending message to handler
                Log.e(TAG, "Sending message to handler.");
                //sending the message to handler in MainActivity
                this.handler.sendMessage(msg);
            }
        }
    }
}

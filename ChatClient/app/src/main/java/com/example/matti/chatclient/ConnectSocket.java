package com.example.matti.chatclient;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by Matti on 19.9.2015.
 */
public class ConnectSocket implements Runnable {
    private Socket socket;
    private static final String TAG = "ChatClient";


    public ConnectSocket() {
        Log.d(TAG, "Creating a new ConnectSocket object");
        createSocket();
    }

    //returns the socket
    public Socket getSocket() {
        Log.d(TAG, "Returning socket with info." + this.socket.getInetAddress());
        return this.socket;
    }

    //creating a new socket
    private void createSocket() {
        this.socket = new Socket();
        Log.d(TAG, "New socket created");
    }


    @Override
    public void run() {
        Log.e(TAG, "Run method started on ConnectSocket");
        // creating a new InetSocketAddress
        InetSocketAddress serverAddress = new InetSocketAddress("192.168.1.187", 100);
        Log.e(TAG, "New InetSocketAddress created");
        try {
            //connect the socket to the inetsocketaddress
            this.socket.connect(serverAddress);
            Log.e(TAG, "Connection has been established.");
            Log.e(TAG, "Socket info in ConnectSocket " + this.socket.getInetAddress());
            Log.e(TAG, "Socket port in ConnectSocket " + this.socket.getLocalPort());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

package com.example.matti.chatclient;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Created by Matti on 18.9.2015.
 */
public class MessageSender implements Runnable {
    private String message = "";
    private Socket socket;
    private BufferedWriter out;
    private static final String TAG = "ChatClient";

    //getting socket and user written message from MainActivity
    public MessageSender(String message, Socket socket) {
        this.message = message;
        this.socket = socket;
        Log.e(TAG, "A new message sender object created with message " + this.message);
        Log.e(TAG, "Socket info in MessageSender " + this.socket.getInetAddress());
        Log.e(TAG, "Socket port in MessageSender " + this.socket.getLocalPort());


    }

    @Override
    public void run() {
        Log.e(TAG, "Run method started in MessageSender");

        try {
            out = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
            Log.e(TAG, "Sending message " + this.message);
            out.write(this.message + "\n");
            Log.e(TAG, "Flushing bufferedWriter");
            out.flush();
            Log.e(TAG, "Closing bufferedWriter");
            //out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

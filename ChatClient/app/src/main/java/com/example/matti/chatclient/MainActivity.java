package com.example.matti.chatclient;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.net.Socket;
import java.util.logging.LogRecord;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "ChatClient";
    MessageSender messageSender;
    ConnectSocket connectSocket;
    Socket socket;


    TextView textBox;
    ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate started.");
        super.onCreate(savedInstanceState);
        //getting the view
        setContentView(R.layout.activity_main);
        //finding the TextView
        textBox = (TextView) findViewById(R.id.text_box);
        //finding the ScrollView
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        //creating and connecting the socket in a new thread
        connectSocket = new ConnectSocket();
        Thread g = new Thread(connectSocket);
        Log.e(TAG, "Started new thread for ConnectSocket.");
        g.start();
        Log.e(TAG, "Thread for ConnectSocket ended.");
        //socket = connectSocket.getSocket();
        Log.e(TAG, "Received socket from connectSocket.");

        //checking if the socket is created and connected before continuing
        while (true) {
            if (!g.isAlive()) {
                //receiving socket from connectSocket
                socket = connectSocket.getSocket();
                //creating a new thread for the MessageReceiver
                Thread t = new Thread(new MessageReceiver(handler, socket));
                Log.e(TAG, "Starting a new thread for MessageReceiver.");
                t.start();
                break;
            }
            else {
                Log.e(TAG, "ConnectSocket thread still running.");
            }
        }

    }

    // creating a new handler Anonymous class
    private Handler handler = new Handler() {
        // creating a method for getting out the string
        // from the Message object

        public void handleMessage(Message msg) {
            String newLine = "\n";
            if (msg.what == 0) {
                //setting text size on textBox
                textBox.setTextSize(10);
                //appending line on textBox
                textBox.append((String) msg.obj);
                Log.e(TAG, "Appending the text box with the new line");
                Log.e(TAG, "The line to append is " + msg.obj);
                //appending with a new line
                textBox.append(newLine);
                //scrolling to the bottom of the scrollView
                scrollToBottom();
            }
        }
    };

    private void scrollToBottom()
    {
        scrollView.post(new Runnable() {
            public void run() {
                //getBottom returns the value to which the scrollView smoothly scrolls to
                scrollView.smoothScrollTo(0, scrollView.getBottom());
            }
        });
    }



    public void sendMessage(View view) {
        Log.e(TAG, "Send message button was clicked");
        //finding the editText View
        EditText editText = (EditText) findViewById(R.id.edit_message);
        //getting the userinput from the edittext
        String messageToSend = editText.getText().toString();
        messageSender = new MessageSender(messageToSend, socket);
        Thread s = new Thread(messageSender);
        Log.e(TAG, "Creating a new thread for MessageSender and starting it");
        s.start();
        editText.setText("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}